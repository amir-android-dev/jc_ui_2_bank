package com.amir.jc_ui_2_bank.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_2_bank.R
import com.amir.jc_ui_2_bank.data.BankCardMockData
import com.amir.jc_ui_2_bank.util.ColorUtil
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import dev.chrisbanes.snapper.rememberSnapperFlingBehavior

@Composable
@OptIn(ExperimentalMaterialApi::class, ExperimentalSnapperApi::class)
fun CardBankView() {
    val lazyState = rememberLazyListState()
    LazyRow(
        contentPadding = PaddingValues(25.dp),
        state = lazyState,
        flingBehavior = rememberSnapperFlingBehavior(lazyState),
        horizontalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        itemsIndexed(BankCardMockData.registeredCards()) { index, item ->
            Card(
                modifier = Modifier
                    .width(280.dp)
                    .height(160.dp),
                onClick = {},
                shape = RoundedCornerShape(15.dp),
                elevation = 8.dp
            ) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .clip(RoundedCornerShape(15.dp))
                ) {
                    //card
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .background(color = Color(ColorUtil.getRandomColor()))
                    ) {
                        //top-> cardType & Logo
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .padding(20.dp, 20.dp, 20.dp, 15.dp),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(text = item.cardType, fontSize = 14.sp)
                            Image(
                                painter = painterResource(id = R.drawable.master),
                                contentDescription = "Card Type",
                                modifier = Modifier.size(40.dp)
                            )
                        }
                        //Card number
                        Box(
                            Modifier
                                .fillMaxWidth()
                                .padding(5.dp, 5.dp, 5.dp, 20.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = item.nr, color = Color.Black, fontSize = 20.sp)
                        }
                        //Card owner & expire time
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .background(Color.Black)
                                .padding(25.dp, 6.dp),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(text = item.owner, color = Color.White)
                            Text(text = item.expiredTime, color = Color.White)
                        }

                    }
                }
            }
            //to have horizontally space between cards
            // Spacer(modifier = Modifier.width(10.dp))
        }
    }
}