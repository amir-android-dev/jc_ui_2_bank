package com.amir.jc_ui_2_bank.data


import co.yml.charts.ui.linechart.model.LineChartData
import co.yml.charts.ui.linechart.model.LinePlotData
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import co.yml.charts.axis.AxisData
import co.yml.charts.common.model.Point
import co.yml.charts.ui.linechart.model.GridLines
import co.yml.charts.ui.linechart.model.IntersectionPoint
import co.yml.charts.ui.linechart.model.Line
import co.yml.charts.ui.linechart.model.LineStyle
import co.yml.charts.ui.linechart.model.SelectionHighlightPoint
import co.yml.charts.ui.linechart.model.SelectionHighlightPopUp
import co.yml.charts.ui.linechart.model.ShadowUnderLine
import com.amir.jc_ui_2_bank.ui.theme.AppButtonDark


class ChartsData {

    companion object {
        //points
        private val pointsData: List<Point> =
            listOf(
                Point(0f, 0f),
                Point(1f, 10f),
                Point(2f, 27f),
                Point(3f, 35f),
                Point(4f, 27f),
                Point(5f, 15f),
                Point(6f, 25f),
                Point(7f, 38f)
            )

        //x line
        private val xAxisData = AxisData.Builder()
            .backgroundColor(AppButtonDark)
            .axisStepSize(100.dp)
            .steps(pointsData.size - 1)
            .labelData { i ->
                val months = listOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug")
                months[i]
            }
            .labelAndAxisLinePadding(15.dp)
            .build()

        //y line
        private val yAxisData = AxisData.Builder()
            .steps(pointsData.size)
            .backgroundColor(AppButtonDark)
            .labelData { i-> i.toString() }
            //.labelAndAxisLinePadding(5.dp)
            .build()

        //lineChar
        val lineChartData = LineChartData(
            linePlotData = LinePlotData(
                lines = listOf(
                    Line(
                        dataPoints = pointsData,
                        LineStyle(),
                        IntersectionPoint(),
                        SelectionHighlightPoint(),
                        ShadowUnderLine(),
                        SelectionHighlightPopUp()
                    )
                ),
            ),
            xAxisData = xAxisData,
            yAxisData = yAxisData,
            gridLines = GridLines(),
            backgroundColor = Color.White
        )
    }

}