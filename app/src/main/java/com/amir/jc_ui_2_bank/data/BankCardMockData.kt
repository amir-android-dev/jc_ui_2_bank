package com.amir.jc_ui_2_bank.data

import com.amir.jc_ui_2_bank.model.BankCardModel

class BankCardMockData {

    companion object {
        private val cards = mutableListOf<BankCardModel>(
            BankCardModel("5578 1268 8797 2145", "12/2027", "Ema Ho.", "Debit Card"),
            BankCardModel("5578 9874 6574 1308", "1/2028", "Anna Ho.", "Credit Card"),
            BankCardModel("5578 8745 57432 4985", "11/2025", "Friends Best", "Visa Card"),
        )


        fun registeredCards() = cards
    }
}