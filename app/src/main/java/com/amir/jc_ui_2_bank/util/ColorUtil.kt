package com.amir.jc_ui_2_bank.util

import android.graphics.Color
import java.util.Random


class ColorUtil {

    companion object {
        fun getRandomColor(): Int {
            val rnd = Random()
            //0 is black & 255 is white. in order to have better color variation we choose between 80 & 200
            val low = 80
            val high = 200

            return Color.argb(
                255,
                rnd.nextInt(high - low) + low,
                rnd.nextInt(high - low) + low,
                rnd.nextInt(high - low) + low
            )
        }
    }
}