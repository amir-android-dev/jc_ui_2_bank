package com.amir.jc_ui_2_bank.model

data class BankCardModel(
    val nr: String,
    val expiredTime: String,
    val owner: String,
    val cardType: String
)
