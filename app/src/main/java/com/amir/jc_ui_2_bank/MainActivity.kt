package com.amir.jc_ui_2_bank

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.amir.jc_ui_2_bank.ui.screen.BankBalanceView
import com.amir.jc_ui_2_bank.ui.screen.CardBankView
import com.amir.jc_ui_2_bank.ui.screen.ChartView
import com.amir.jc_ui_2_bank.ui.screen.TopMenuView
import com.amir.jc_ui_2_bank.ui.theme.Jc_ui_2_bankTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Jc_ui_2_bankTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainView()
                }
            }
        }
    }
}

@Composable
fun MainView() {

    Box(Modifier.fillMaxSize()) {
        Column(Modifier.fillMaxWidth()) {
            TopMenuView()
            BankBalanceView()
            Spacer(modifier = Modifier.height(15.dp))
            CardBankView()
            ChartView()
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Jc_ui_2_bankTheme {
        MainView()
    }
}